# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of MESH_GENERATION.
#
# MESH_GENERATION is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MESH_GENERATION is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MESH_GENERATION. If not, see <http://www.gnu.org/licenses/>.



import os


endo_lv = "biv_mesh_endo_lv.ply"
endo_rv = "biv_mesh_endo_rv.ply"
epi = "biv_mesh_epi.ply"
out = "biv_mesh.msh"



fiber_markers_biv = {'BASE':10, 'ENDO_RV': 20, 'ENDO_LV': 30, 'EPI': 40, \
    'WALL': 50, 'ENDORING_RV': 200, 'ENDORING_LV': 300, \
        'EPIRING': 400, 'WALL':50}


def build_gmsh_files():
    "Mesh *ply files together"
    os.system("gmsh -3 {} -merge {} {} biv_mesh.geo -o {}".format(endo_lv, endo_rv, epi, out))

from textwrap import dedent

def build_dolfin_files():
    dolfin_path = "output_file_in_fenics.h5"
    from dolfin import (FunctionSpace, File, facets,
                        mpi_comm_world, MeshFunction)
    from mesh_generation.mesh_utils import (generate_fibers,
                                            get_fiber_markers)
    
    from mesh_generation.gmsh import GmshFile, gmsh2dolfin, load_geometry
    from fiberrules import dolfin_fiberrules

    fiber_markers = get_fiber_markers()
    gmsh = GmshFile("biv_mesh.msh", marker_ids = fiber_markers)
    mesh, markers = gmsh2dolfin(gmsh, mpi_comm_world())

    ffun_ = MeshFunction("size_t", mesh, 2, mesh.domains())

    mapping = {2:30, # LV
               max(ffun_.array()):0, #interior
               3:20, # RV
               10:10, #Base
               40:40} #Epi
               
    for f in facets(mesh):
        mesh.domains().set_marker((f.index(), mapping[ffun_[f]]), 2)

    ffun = MeshFunction("size_t", mesh, 2, mesh.domains())

    fib= generate_fibers(mesh)

    f = File("biv_mesh.xml")
    f << mesh

    f = File("facet_function.xml")
    f << ffun

    f = File("fibers.xml")
    f << fib[0]

    f = File("sheets.xml")
    f << fib[1]

    f = File("cross-sheets.xml")
    f << fib[2]


def save_mesh_as_vtk():
    
    from dolfin import FunctionSpace, File
    from fenicshotools import load_geometry

    for t in range(1,16):
        domain, marker = load_geometry(mpi_comm_world(), dolfin_path, "Frame_{}".format(t))

        vtk_file = File("vtk_file.pvd".format(t))
        vtk_file << domain.data(), float(t)


if __name__ == "__main__": 
    # build_gmsh_files()
    build_dolfin_files()
    #save_mesh_as_vtk()
