# System imports
from distutils.core import setup
import platform
import sys
from os.path import join as pjoin

# Version number
major = 0
minor = 1

scripts = [pjoin("scripts", "echo2dolfin")]

if platform.system() == "Windows" or "bdist_wininst" in sys.argv:
    # In the Windows command prompt we can't execute Python scripts
    # without a .py extension. A solution is to create batch files
    # that runs the different scripts.
    batch_files = []
    for script in scripts:
        batch_file = script + ".bat"
        f = open(batch_file, "w")
        f.write('python "%%~dp0\%s" %%*\n' % split(script)[1])
        f.close()
        batch_files.append(batch_file)
    # scripts.extend(batch_files)


setup(name = "mesh_generation",
      version = "{0}.{1}".format(major, minor),
      description = """
      
      """,
      
      author = "Henrik Finsberg",
      
      author_email = "henriknf@simula.no",
      
      packages = ["mesh_generation",
                  "mesh_generation.gmsh",
                  "mesh_generation.idealized_geometry"],
      
      package_dir = {"mesh_generation": "mesh_generation"},
      scripts = scripts,

      )
