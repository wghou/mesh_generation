# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of MESH_GENERATION.
#
# MESH_GENERATION is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MESH_GENERATION is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MESH_GENERATION. If not, see <http://www.gnu.org/licenses/>.
import dolfin
import numpy as np
from mesh_generation.strain_regions import make_crl_basis, mark_cell_function
from mesh_generation.mesh_utils import save_geometry_to_h5, add_fibers_to_h5, get_fiber_markers, generate_fibers, get_markers, logger, load_geometry_from_h5
from mesh_generation.generate_mesh import setup_fiber_parameters
import sys

ellipsoid_types = ["prolate", "ellipsoid"]
ndivs = [1,2,3]
orders = [1,2]
axisymetrics = [False, True]
interpolates = [True, False]

def get_focal(par):

    if par.has_key("d_focal"):
        return par["d_focal"]
    else:
        l = par["r_long_endo"]
        s = par["r_short_endo"]
        return np.sqrt(l**2 - s**2)
    
    

def mark_strain_regions(mesh, foc, nsectors=[6,6,4,1], mark_mesh = True):
    """Mark the cells in the mesh.

    For instance if you want to mark this mesh accoring to the 
    AHA 17-segment model, then nsector = [6,6,4,1], 
    i.e 6 basal, 6 mid, 4 apical and one apex

    :param mesh: 
    :param foc: 
    :param nsectors: 
    :param mark_mesh: 
    :returns: 
    :rtype: 

    """
    
    
    fun = dolfin.MeshFunction("size_t", mesh, 3)
    nlevels = len(nsectors)

    pi = np.pi

    assert nlevels <= 4

    if nlevels == 4:
        mus = [90,60,30, 10, 0]
    elif nlevels == 3:
        mus = [90,60,30, 0]
    elif nlevels == 2:
        mus = [90,45, 0]
    else:
        mus = [90,0]
   

    thetas = [np.linspace(pi,3*pi, s+1)[:-1].tolist() + [pi] for s in nsectors]

    start = 0
    end = nsectors[0]
    regions = np.zeros((sum(nsectors), 4))
    for i in range(nlevels):
        regions.T[0][start:end] = mus[i]*pi/180
        regions.T[3][start:end] = mus[i+1]*pi/180
        if i != len(nsectors)-1:
            start += nsectors[i]
            end += nsectors[i+1]
        
    start = 0
    for j, t in enumerate(thetas):
        for i in range(nsectors[j]):
            regions.T[1][i+start] = t[i]
            regions.T[2][i+start] = t[i+1]
        start += nsectors[j]

   

    sfun = mark_cell_function(fun, mesh, foc, regions)
  
  
    if mark_mesh:
        # Mark the cells accordingly
        for cell in dolfin.cells(mesh):
            mesh.domains().set_marker((cell.index(), sfun[cell]), 3)
            
    return sfun
            
        
def mark_boundaries_for_fiberrules(geo):
    """Mark the facets on the mesh accoring to 
    preferences from fiberrules, i.e the modulue 
    that generates the fiber fields

    :param object geo: Object with mesh, and marker values 
    :returns: The new markers 
    :rtype: dict

    """
    

    markers = get_fiber_markers("lv")
    
    mesh = geo.mesh

    # Move the base to x = 0
    p = dolfin.Point(np.array([-np.min(mesh.coordinates(), 0)[0], 0, 0]))
    mesh.translate(p)
    
    bfun = dolfin.MeshFunction("size_t", mesh, 2, mesh.domains())
    rfun = dolfin.MeshFunction("size_t", mesh, 1, mesh.domains())

    for f in dolfin.facets(mesh):
        if bfun[f] == geo.ENDO:
            mesh.domains().set_marker((f.index(), markers["ENDO"]), 2)
        elif bfun[f] == geo.EPI:
            mesh.domains().set_marker((f.index(), markers["EPI"]), 2)
        elif bfun[f] == geo.BASE:
            mesh.domains().set_marker((f.index(), markers["BASE"]), 2)

        for e in dolfin.edges(f):
            if rfun[e] == geo.ENDORING:
                mesh.domains().set_marker((e.index(), markers["ENDORING"]), 1)
            elif rfun[e] == geo.EPIRING:
                mesh.domains().set_marker((e.index(), markers["EPIRING"]), 1)

    return get_markers("lv")


def create_geometry(ellipsoid_type, ndiv, order,
                    axisymetric, interpolate, tag,
                    save_mesh=True):
    """
    Creates a geometry with a given set of parameters.
    """
    

    try:
        from pulse import LVProlateEllipsoid, LVSimpleEllipsoid
        has_pulse = True
    except:
        msg = "\nWarning: Pulse is not installed. \n"\
          "Please install pulse in order to use "\
          "idealized geometry module. \n" \
          "git clone git@bitbucket.org:simula-camo/pulse.git\n"
        
        logger.debug(msg)
        has_pulse = False
        sys.exit()
    
    # Pick ellipsoid class
    if ellipsoid_type == "prolate":
        LVEllipsoid = LVProlateEllipsoid
    else:
        LVEllipsoid = LVSimpleEllipsoid

    geoparam = LVEllipsoid.default_parameters()
    geoparam['mesh_generation']['ndiv'] = ndiv
    geoparam['mesh_generation']['order'] = order
    geoparam['axisymmetric'] = axisymetric
    # geoparam['microstructure']['interpolate'] = interpolate
    

    # meshname = "./data/mesh_{}_{}_{}_{}_{}_{}.h5".format(\
    #     ellipsoid_type, ndiv, order, int(axisymetric), int(interpolate), tag) \
    #     if save_mesh else ""

    
    geom = LVEllipsoid("", "", dolfin.mpi_comm_world(), False, geoparam)

    geom.parameters = geoparam
    # geom.h5name = meshname

    return geom


def generate_data_adjoint_contraction(ellipsoid_type = "prolate", ndiv = 1, order = 1,
                                      axisymetric = False, interpolate = False, tag = "",
                                      nsectors = [6,6,4,1]):
    
    geo = create_geometry(ellipsoid_type, ndiv, order, axisymetric, interpolate, tag)

    mesh = geo.mesh
  


    markers = mark_boundaries_for_fiberrules(geo)

    foc = get_focal(geo.parameters["geometry"])
    
    c,r,l = make_crl_basis(mesh, foc)
    local_basis = [c,r,l]

    
    fiber_params = setup_fiber_parameters()
    fiber_params["include_sheets"] =  True

    
    fiber_params["fiber_angle_endo"] = 60
    fiber_params["fiber_angle_epi"] = -60
    
    fields = generate_fibers(mesh, fiber_params)
    
    h5name = "./data/mesh_{}_{}_{}_{}_{}_{}.h5".format(\
        ellipsoid_type, ndiv, order, int(axisymetric), int(interpolate), tag)
    h5group = ""

    mark_strain_regions(mesh, foc, nsectors)

    
    
    save_geometry_to_h5(mesh, h5name, h5group,
                        markers, fields, local_basis, overwrite_file=True)
    
    # fiber_params["fiber_angle_endo"] = 60
    # fiber_params["fiber_angle_epi"] = 60
    # fields = generate_fibers(mesh, fiber_params)
    # add_fibers_to_h5(h5name, h5group, fields)

    return load_geometry_from_h5(h5name, h5group), foc
    

if __name__ == "__main__":

    ellipsoid_type = "simple"
    ndiv =1
    order = 1
    axisymetric = False
    interpolate = False
    tag = ""

                    
    generate_data_adjoint_contraction()

