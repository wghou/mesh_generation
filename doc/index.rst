.. Mesh Generation documentation master file, created by
   sphinx-quickstart on Thu Dec  1 21:44:53 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mesh Generation's documentation!
===========================================

This is a module for automatic mesh generation of HDF5 files
coming from GEVU software echopac. The output format is meshes
that can be used for finite element computation in FeniCS

Installation and dependencies:
------------------------------

The pulse-adjoint source code is hosted on Bitbucket:

  https://bitbucket.org/finsberg/mesh_generation

mesh_generation is based on

* The FEniCS Project software (http://www.fenicsproject.org)


Main authors:
-------------

  * Henrik Finsberg    (henriknf@simula.no)

API documentation:
------------------

.. toctree::
   :maxdepth: 2

   mesh_generation
   mesh_generation.idealized_geometry
   mesh_generation.gmsh


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

