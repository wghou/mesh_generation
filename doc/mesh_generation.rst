mesh_generation package
=======================

Subpackages
-----------

.. toctree::

    mesh_generation.gmsh
    mesh_generation.idealized_geometry

Submodules
----------

mesh_generation.generate_mesh module
------------------------------------

.. automodule:: mesh_generation.generate_mesh
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.mesh_utils module
---------------------------------

.. automodule:: mesh_generation.mesh_utils
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.strain_regions module
-------------------------------------

.. automodule:: mesh_generation.strain_regions
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.surface module
------------------------------

.. automodule:: mesh_generation.surface
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mesh_generation
    :members:
    :undoc-members:
    :show-inheritance:
