from mesh_generation.mesh_utils import *
from dolfin import *
from dolfin_adjoint import interpolate

def test_save():

    mesh = UnitCubeMesh(2,2,2)

    dir_sub = CompiledSubDomain("near(x[1], 0)")
    neu_sub = CompiledSubDomain("near(x[1], 1)")
    ffun = MeshFunction("size_t", mesh, 2)
    ffun.set_all(0)
    dir_sub.mark(ffun, 1)
    neu_sub.mark(ffun, 2)

    dom1 = CompiledSubDomain("x[0] <= 0.5")
    dom2 = CompiledSubDomain("x[0] >= 0.5")
    cfun =  MeshFunction("size_t", mesh, 3)
    cfun.set_all(0)
    dom1.mark(cfun, 1)
    dom2.mark(cfun, 2)

    markers = {"surf1":(1,2),
               "surf2":(2,2),
               "dom1":(1,3),
               "dom2":(2,3)}
               
               
    

    W_cg1 = FunctionSpace(mesh, "CG", 1)
    V_cg1 = VectorFunctionSpace(mesh, "CG", 1)
    quad_elm = VectorElement(family = "Quadrature",
                             cell = mesh.ufl_cell(),
                             degree = 4,
                             quad_scheme="default")
    V_f = FunctionSpace(mesh, quad_elm)
    
    # Local basis
    e_circ = interpolate(Expression(("0.0", "1.0", "0.0")), V_cg1, name = "circ")
    e_long = interpolate(Expression(("1.0", "0.0", "0.0")), V_cg1, name = "long")
    e_rad = interpolate(Expression(("0.0", "0.0", "1.0")), V_cg1, name = "rad")
    local_basis = [e_circ, e_long, e_rad]
    
    # Fiber-sheet system
    f0 = interpolate(Expression(("1/sqrt(3)",  "1/sqrt(3)",  "1/sqrt(3)")), V_f, name = "f0")
    s0 = interpolate(Expression(("1/sqrt(2)",  "0.0",  "-1/sqrt(2)")), V_f, name = "s0")
    n0 = interpolate(Expression(("1/sqrt(6)",  "2/sqrt(6)",  "-1/sqrt(6)")), V_f, name = "n0")
    fields = [f0, s0, n0]

    u = interpolate(Expression(("0.03", "0.01", "-0.02")), V_cg1, name = "my_displacement")
    f = interpolate(Expression("x[0]"), W_cg1, name = "my_scalar")
    other_functions = {"displacement":u, "scalar":f}


    h5name = "test_save.h5"

    h5group = "test1"
    save_geometry_to_h5(mesh, h5name, h5group, markers,
                        fields, local_basis, mesh.mpi_comm(),
                        other_functions)
    geo = load_geometry_from_h5(h5name, h5group)
    
    h5group = "test2"
    save_geometry_to_h5(mesh, h5name, h5group)
    geo = load_geometry_from_h5(h5name, h5group)

    h5group = "test3"
    save_geometry_to_h5(mesh, h5name, h5group, fields = [], local_basis = [])
    geo = load_geometry_from_h5(h5name, h5group)

    
    
    

if __name__ == "__main__":
    test_save()
