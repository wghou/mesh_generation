# Mesh Generation #

This repo contains code for generating heart meshes.

## How to generate patient meshes from 4D Echo ##

Change parameters in the script generate_mesh.py.
To generate the mesh, just do
```
#!python

python generate_mesh.py
```

## How to generate idealized geometries ##

```
#!python

python idealized_geometries/generate_idealized_geometry.py
```
Change parameters in the file according to your desire.

## Features ##
* Rule-based fibers.
* Marking accoring to AHA-zones (16 or 17 segements).
* Mesh refinement (local or global).
* Local basis in circumferential, radial and longitudinal direction.
* A cut is made at the base, based on a least square fitting of a basal plane to the basal points on the strain mesh. The cut is then adjusted so that the cavity volume agrees with the measured volume.
* The pipeline is fully automated. 

## Requirements ##
```
* FEniCS version 2016.1
  -- http://fenicsproject.org
* Gmsh
  -- Build Gmsh with shared libraires
  -- http://gmsh.info
```
Optional
```
* Gamer (Requires Blender)
  -- Used for smoothing surfaces
  -- wget folk.uio.no/hake/fetk/gamer-install.sh
* fiberrules
  -- Used for assigning rule based fiber orientations according to the algorithm outlined in [1]
  -- https://bitbucket.org/johanhake/fiberrules
* pulse
  -- Used for creating idealized geometries
  -- https://bitbucket.org/simula-camo/pulse
```



[1] Bayer, J. D., et al. "A novel rule-based algorithm for assigning myocardial fiber orientation to computational heart models." Annals of biomedical engineering 40.10 (2012): 2243-2254.